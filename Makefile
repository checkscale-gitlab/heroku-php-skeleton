-include .env


## Initialize after cloning this repository -


.PHONY: init


init:
	cp -n .env.example .env
	docker-compose exec php composer install


## Docker controll ---------------------


.PHONY: up build down stop destroy rebuild ps


up:
	docker-compose up -d
	@make ps

build:
	docker-compose build

down:
	docker-compose down

stop:
	docker-compose stop

destroy:
	docker-compose down --rmi all --volumes

rebuild:
	@make destroy
	@make build

ps:
	docker-compose ps


## Per-container operations ------------


.PHONY: logs-php bash-php restart-php printenv-php \
	logs-nginx bash-nginx restart-nginx printenv-nginx


logs-php:
	docker-compose logs --tail="10" php

bash-php:
	docker-compose exec php bash

restart-php:
	docker-compose restart php

printenv-php:
	docker-compose exec php printenv


logs-nginx:
	docker-compose logs --tail="10" nginx

bash-nginx:
	docker-compose exec nginx bash

restart-nginx:
	docker-compose restart nginx

printenv-nginx:
	docker-compose exec nginx printenv


## Heroku operations -------------------


.PHONY: heroku-login heroku-apps \
	heroku-bash-stg heroku-restart-stg heroku-logs-stg \
	heroku-bash-prd heroku-restart-prd heroku-logs-prd \


heroku-login:
	docker-compose exec php heroku login -i

heroku-apps:
	docker-compose exec php heroku apps


heroku-bash-stg:
	docker-compose exec php heroku run bash -a $(HEROKU_STG_APP_NAME)

heroku-restart-stg:
	docker-compose exec php heroku restart -a $(HEROKU_STG_APP_NAME)

heroku-logs-stg:
		docker-compose exec php heroku logs --tail -a $(HEROKU_STG_APP_NAME)


heroku-bash-prd:
	docker-compose exec php heroku run bash -a $(HEROKU_PRD_APP_NAME)

heroku-restart-prd:
	docker-compose exec php heroku restart -a $(HEROKU_PRD_APP_NAME)

heroku-logs-prd:
		docker-compose exec php heroku logs --tail -a $(HEROKU_PRD_APP_NAME)
